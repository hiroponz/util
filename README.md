GitLabトレーニングの環境の構築手順です。

まずは、下記を参考にVitrualBoxにUbuntu 18.04をインストールしてください。

https://www.sejuku.net/blog/82291

続いてサイドバーの「端末」を開いて下記のコマンドを実行してください。

```
curl -s https://gitlab.com/hiroponz/util/raw/master/setup-ubuntu.sh | bash -
```

## 動作確認

端末で以下のコマンドを実行して、ツールが正しくインストールされたことを確認してください。

git:

```
git --version
```

docker:

```
docker pull hello-world
```

docker-compose:

```
docker-compose --version
```

visual studio code:

```
code
```
